package com.example.wsrkazan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wsrkazan.adapters.ViewPagerTutorial
import com.example.wsrkazan.databinding.ActivityTutorialBinding
import com.example.wsrkazan.network.TutorialClass
import com.google.android.material.tabs.TabLayoutMediator

class TutorialActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTutorialBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTutorialBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val listResult: List<TutorialClass> = listOf(
            TutorialClass(R.drawable.viewphotofirst, "Select the quest!", "A huge collection of different quests. Historical,\n children's, outdoors and many others..."),
            TutorialClass(R.drawable.viewphotosecond, "Complete the task!", "Search for secret keys, location detection, step\n counting and much more..."),
            TutorialClass(R.drawable.viewphotothree, "Become a Top Key Finder", "User ratings, quest ratings, quest author ratings...")
        )

        binding.viewPagerTutorial.adapter = ViewPagerTutorial(this, listResult)
        TabLayoutMediator(binding.tabLayoutTutorial, binding.viewPagerTutorial) {_,_->}.attach()

        binding.nextButtonTutorial.setOnClickListener {
            if (binding.viewPagerTutorial.currentItem == 1) {
                binding.nextButtonTutorial.text = "Done"

            }
            else if (binding.viewPagerTutorial.currentItem == 2){
                openLoginActivity()
            }
            binding.viewPagerTutorial.currentItem++
        }

        binding.skipButtonTutorial.setOnClickListener {
            openLoginActivity()
        }
    }

    private fun openLoginActivity() {
        val intent = Intent(this, SignInActivity::class.java)
        startActivity(intent)
        finish()
    }
}