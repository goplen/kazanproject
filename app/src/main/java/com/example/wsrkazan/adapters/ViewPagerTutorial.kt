package com.example.wsrkazan.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wsrkazan.R
import com.example.wsrkazan.network.TutorialClass

class ViewPagerTutorial(val context: Context, val list: List<TutorialClass>): RecyclerView.Adapter<ViewPagerTutorial.MyVH>() {
    class MyVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textMain: TextView =itemView.findViewById(R.id.text_main_tutorial)
        val textSecond: TextView = itemView.findViewById(R.id.text_second_tutorial)
        val mainImage: ImageView = itemView.findViewById(R.id.image_main_tutorial)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.view_pager_adapter, parent, false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.textMain.text = list[position].textMain
        holder.textSecond.text = list[position].textSecond
        holder.mainImage.setImageResource(list[position].image)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}